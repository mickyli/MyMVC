package com.freedom.web.mymvc.common;

import com.freedom.web.mymvc.view.ModelAndView;

import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;

public interface SystemInterface {
	// 实现类实现此方法时，不要涉及到局部变量,保证线程安全性
	public ModelAndView service(FullHttpRequest request, FullHttpResponse response)throws Exception;
}
