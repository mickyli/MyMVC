package com.freedom.web.mymvc.route;

public enum RouteType {
	FULL_MATCH, PREFIX_MATCH, SUFFIX_MATCH
}
