package com.freedom.web.mymvc.utils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
/**
 * 
 * @author zhiqiang.liu
 * @2016年5月16日
 *
 */
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;

public class PropertiesUtils {
	private static final Logger logger = LogManager.getLogger(PropertiesUtils.class);

	// 以下为全局需要

	private static PropertiesUtils myProperties = null;// 全局单例变量，一开始就存在

	static {// 静态块里，只加载一次

		Properties props = new Properties();
		try {
			InputStream in = new BufferedInputStream(new FileInputStream(ConstantsUtils.CONFIG_FILE));
			// Thread.currentThread().getContextClassLoader().getResourceAsStream(MyConstants.CONFIG_FILE);
			props.load(in);
			in.close();
		} catch (Exception e) {
			LoggerUtils.error(logger, "fail to read config file " + ConstantsUtils.CONFIG_FILE);
			System.exit(-1);
		}
		// 读取值
		LoggerUtils.debug(logger, "succeed to read config file " + ConstantsUtils.CONFIG_FILE);
		// netty
		int netty_port = Integer.parseInt(props.getProperty(ConstantsUtils.NETTY_PORT, "10000"));
		int netty_boss = Integer.parseInt(props.getProperty(ConstantsUtils.NETTY_BOSS, "1"));
		int netty_worker = Runtime.getRuntime().availableProcessors()
				* Integer.parseInt(props.getProperty(ConstantsUtils.NETTY_WORKER, "2").trim());// 2倍cpu
		// consumer
		int consumer_worker = Runtime.getRuntime().availableProcessors()
				* Integer.parseInt(props.getProperty(ConstantsUtils.CONSUMER_WORKER, "6").trim());// 6倍cpu

		// max size:default 1M
		int max_size_of_http_request = Integer
				.parseInt(props.getProperty(ConstantsUtils.MAX_SIZE_OF_HTTP_REQUEST, "1048576").trim());

		// development mode?
		int development_mode = Integer.parseInt(props.getProperty(ConstantsUtils.DEVELOPMENT_MODE, "0").trim());
		//
		props = null;
		// 构造新的对象
		myProperties = new PropertiesUtils(netty_port, netty_boss, netty_worker, consumer_worker,
				max_size_of_http_request,development_mode);
		LoggerUtils.debug(logger, "succeed to create my properties object ");
	}

	public static PropertiesUtils getInstance() {
		return myProperties;
	}

	// 私有属性开始//////////////////////////////////////////////////////////////////
	// netty
	private int nettyPort;
	private int nettyBoss;
	private int nettyWorker;
	// consumer worker
	private int consumerWorker;
	// max size
	private int maxSize;
	//develop mode
	int developMode;

	private PropertiesUtils() {// 私有方法，保证单例

	}

	private PropertiesUtils(int np, int nboss, int nworker, int cworker, int mSize,int dMode) {

		// used by netty
		this.nettyPort = np;
		this.nettyBoss = nboss;
		this.nettyWorker = nworker;
		this.consumerWorker = cworker;
		this.maxSize = mSize;
		this.developMode=dMode;
	}

	public int getMaxSize() {
		return maxSize;
	}

	public int getConsumerWorker() {
		return this.consumerWorker;
	}

	public int getNettyPort() {
		return nettyPort;
	}

	public int getDevelopMode() {
		return developMode;
	}

	public int getNettyBoss() {
		return nettyBoss;
	}

	public int getNettyWorker() {
		return nettyWorker;
	}

	public String toString() {
		StringBuilder strBuilder = new StringBuilder("\n");
		strBuilder.append(ConstantsUtils.NETTY_PORT).append(": ").append(nettyPort).append("\n");
		strBuilder.append(ConstantsUtils.NETTY_BOSS).append(": ").append(nettyBoss).append("\n");
		strBuilder.append(ConstantsUtils.NETTY_WORKER).append(": ").append(nettyWorker).append("\n");
		strBuilder.append(ConstantsUtils.CONSUMER_WORKER).append(": ").append(consumerWorker).append("\n");
		strBuilder.append(ConstantsUtils.MAX_SIZE_OF_HTTP_REQUEST).append(": ").append(maxSize).append("\n");
		strBuilder.append(ConstantsUtils.DEVELOPMENT_MODE).append(": ").append(developMode).append("");
		//
		return strBuilder.toString();
	}

	// 测试
	public static void main(String[] args) {
		// just for test
		PropertiesUtils property = PropertiesUtils.getInstance();
		logger.debug(property.toString());
	}
}
