package com.freedom.web.mymvc.utils;

/**
 * 
 * @author zhiqiang.liu
 * @2016年5月16日
 *
 */
public class ConstantsUtils {
	// 路由类
	public static String ROUTE_CLASS = "com.freedom.web.mymvc.route.RouteBuilder";
	// 解析配置文件使用
	public static String CONFIG_FILE = System.getProperty("mvcProperties", "src/main/resources/mvc.properties");
	public static String ROUTE_FILE = System.getProperty("routeProperties", "src/main/resources/route.properties");
	// netty使用
	public static String NETTY_PORT = "netty_server_port";
	public static String NETTY_BOSS = "netty_boss_number";
	public static String NETTY_WORKER = "netty_worker_number";

	// 消费者的线程数
	public static String CONSUMER_WORKER = "consumer_worker_number";

	// 最大HTTP请求大小
	public static String MAX_SIZE_OF_HTTP_REQUEST = "max_size_of_http_request";

	// 响应内容的类型
	// "application/json; charset=utf-8";
	public static String JSON = "application/json";
	public static String JAVASCRIPT = "application/javascript";
	public static String CSS = "text/css";
	public static String HTML = "text/html";
	public static String PNG = "image/png";
	public static String GIF = "image/gif";
	public static String JPG = "image/jpeg";
	public static String SVG = "image/svg+xml";
	public static String SERVER = "Netty (4.0.XX.Final)";

	// 是否开发模式
	public static String DEVELOPMENT_MODE = "development_mode";

}