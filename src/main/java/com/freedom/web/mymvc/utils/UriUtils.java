package com.freedom.web.mymvc.utils;

public class UriUtils {
	public static String tep;

	public static String parseUri(String uri) {
		// 1)null
		if (null == uri) {
			return null;
		}
		// 2)截断?
		int index = uri.indexOf("?");
		if (-1 != index) {
			uri = uri.substring(0, index);
		}
		return uri;
	}

	public static void main(String[] args) {
		String a = "hello? b";
		String b = parseUri(a);
		System.out.println("a=" + a + "\n b:" + b);
	}
}
