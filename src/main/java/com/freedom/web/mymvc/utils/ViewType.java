package com.freedom.web.mymvc.utils;

public enum ViewType {
	VELOCITY, FREEMARKER, JSP
}
